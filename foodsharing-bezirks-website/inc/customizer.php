<?php
/**
 * WP Bootstrap Starter Theme Customizer
 *
 * @package foodsharing_bezirks_style
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function themeslug_sanitize_checkbox( $checked ) {
    // Boolean check.
    return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

function foodsharing_bezirks_style_customize_register( $wp_customize ) {

    /*Banner*/
    $wp_customize->add_section(
        'header_image',
        array(
            'title' => __( 'Header Banner', 'foodsharing-bezirks-website' ),
            'priority' => 30,
        )
    );


    $wp_customize->add_control(
        'header_img',
        array(
            'label' => __( 'Header Image', 'foodsharing-bezirks-website' ),
            'section' => 'header_images',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'header_bg_color_setting',
        array(
            'default'     => '#fff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'header_bg_color',
            array(
                'label'      => __( 'Header Banner Background Color', 'foodsharing-bezirks-website' ),
                'section'    => 'header_image',
                'settings'   => 'header_bg_color_setting',
            ) )
    );

    $wp_customize->add_setting( 'header_banner_title_setting', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'header_banner_title_setting', array(
        'label' => __( 'Banner Title', 'foodsharing-bezirks-website' ),
        'section'    => 'header_image',
        'settings'   => 'header_banner_title_setting',
        'type' => 'text'
    ) ) );

    $wp_customize->add_setting( 'header_banner_tagline_setting', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'header_banner_tagline_setting', array(
        'label' => __( 'Banner Tagline', 'foodsharing-bezirks-website' ),
        'section'    => 'header_image',
        'settings'   => 'header_banner_tagline_setting',
        'type' => 'text'
    ) ) );
    $wp_customize->add_setting( 'header_banner_visibility', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'themeslug_sanitize_checkbox',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'header_banner_visibility', array(
        'settings' => 'header_banner_visibility',
        'label'    => __('Remove Header Banner', 'foodsharing-bezirks-website'),
        'section'    => 'header_image',
        'type'     => 'checkbox',
    ) ) );

    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

    // foodsharing Bezirk
$wp_customize->add_setting( 'foodsharing_bezirk_title_setting', array(
    'default' => __( 'Bezirksname', 'foodsharing-bezirks-website' ),
    'sanitize_callback' => 'wp_filter_nohtml_kses',
) );
$wp_customize->add_control( new WP_Customize_Control($wp_customize, 'foodsharing_bezirk_title_setting', array(
    'label' => __( 'foodsharing Bezirk', 'foodsharing-bezirks-website' ),
    'section'    => 'title_tagline',
    'settings'   => 'foodsharing_bezirk_title_setting',
    'type' => 'text'
) ) );

    // Add control for logo uploader
    $wp_customize->add_setting( 'foodsharing_bezirks_style_logo', array(
        //'default' => __( '', 'foodsharing-bezirks-website' ),
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'foodsharing_bezirks_style_logo', array(
        'label'    => __( 'Upload Logo (replaces text)', 'foodsharing-bezirks-website' ),
        'section'  => 'title_tagline',
        'settings' => 'foodsharing_bezirks_style_logo',
    ) ) );

}
add_action( 'customize_register', 'foodsharing_bezirks_style_customize_register' );

add_action( 'wp_head', 'foodsharing_bezirks_style_customizer_css');
function foodsharing_bezirks_style_customizer_css()
{
    ?>
    <style type="text/css">
        #page-sub-header { background: <?php echo get_theme_mod('header_bg_color_setting', '#fff'); ?>; }
    </style>
    <?php
}


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function foodsharing_bezirks_style_customize_preview_js() {
    wp_enqueue_script( 'foodsharing_bezirks_style_customizer', get_template_directory_uri() . '/inc/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'foodsharing_bezirks_style_customize_preview_js' );

function remove_styles_sections()
{
    global $wp_customize;
    $wp_customize->remove_control('site_icon');    
}
    
add_action( 'customize_register', 'remove_styles_sections', 20 );